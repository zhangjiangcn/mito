package com.luoxue.mito;

import com.luoxue.mito.util.ImageUtil;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.util.List;

public class ImageUtilTests {
    /**
     * 测试在指定目录下生成缩略图
     */
    @Test
    public void testGenerateThumbnail2Directory() throws IOException {
        String path = "D:\\my\\study-code\\mito\\temps\\vmgirls\\image\\2018\\08";
        String[] files = new String[]{
                "2018-08-10_13-52-47.jpg",
        };

        List<String> list = ImageUtil.generateThumbnail2Directory(path, files);
        System.out.println(list);
    }

    /**
     * 将指定目录下的图片生成缩略图
     */
    @Test
    public void testGenerateDirectoryThumbnail() throws IOException {
        String path = "D:\\my\\study-code\\mito\\temps\\vmgirls\\image\\2018\\08";
        ImageUtil.generateDirectoryThumbnail(path);
    }

    @Test
    public void testSaveFile() throws IOException {
        URL url = new URL("https://img.vm.laomishuo.com/cover/16490.jpg");
        ImageUtil.saveFile(url, "D:\\test" + url.getPath());
    }
}
