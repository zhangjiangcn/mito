package com.luoxue.mito;

import com.luoxue.mito.config.MitoConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableConfigurationProperties(MitoConfig.class)
@EnableAsync
public class MitoApplication {
    public static void main(String[] args) {
        SpringApplication.run(MitoApplication.class, args);
    }

}
