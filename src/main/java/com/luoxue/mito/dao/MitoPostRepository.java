package com.luoxue.mito.dao;

import com.luoxue.mito.entity.MitoPost;
import com.luoxue.mito.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;


public interface MitoPostRepository extends JpaRepository<MitoPost, Long>, JpaSpecificationExecutor<MitoPost> {
    @Modifying
    @Transactional
    @Query(value = "truncate table mito_post", nativeQuery = true)
    void truncate();
}
