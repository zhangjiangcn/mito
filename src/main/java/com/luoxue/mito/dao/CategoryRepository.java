package com.luoxue.mito.dao;

import com.luoxue.mito.entity.Category;
import com.luoxue.mito.entity.MitoPost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


public interface CategoryRepository extends JpaRepository<Category, Long> {
    @Query(value = "select distinct c.name from Category c order by c.name")
    List<String> getAllCategory();

    @Query(value = "select distinct c.tag from Category c where (c.name=?1 or ?1 is null) order by c.tag")
    List<String> getTags(String category);

    @Query(value = "SELECT name, CONCAT( \"[\", GROUP_CONCAT( CONCAT('\"', tag, '\"' )), \"]\" ) tag FROM`category`GROUP BY`name`",nativeQuery = true)
    List<Map<?,?>> list();

    @Modifying
    @Transactional
    @Query(value = "truncate table category", nativeQuery = true)
    void truncate();
}
