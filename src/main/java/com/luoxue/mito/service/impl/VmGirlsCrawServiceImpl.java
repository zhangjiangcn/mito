package com.luoxue.mito.service.impl;

import cn.edu.hfut.dmic.webcollector.util.FileUtils;
import com.luoxue.mito.config.MitoConfig;
import com.luoxue.mito.crawler.VmGirlsCrawler;
import com.luoxue.mito.dao.CategoryRepository;
import com.luoxue.mito.dao.MitoPostRepository;
import com.luoxue.mito.service.IVmGirlsCrawService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;

@Service
public class VmGirlsCrawServiceImpl implements IVmGirlsCrawService {
    @Resource
    private VmGirlsCrawler vmGirlsCrawler;
    @Resource
    MitoConfig mitoConfig;
    @Resource
    private MitoPostRepository mitoPostRepository;
    @Resource
    private CategoryRepository categoryRepository;

    @Override
    @Async
    public void vmGirlsCrawlRun() {
        File file = new File(mitoConfig.getFilePath());
        if (file.exists()) {
            FileUtils.deleteDir(file);
        }
        categoryRepository.truncate();
        mitoPostRepository.truncate();
        try {
            vmGirlsCrawler.start(2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
