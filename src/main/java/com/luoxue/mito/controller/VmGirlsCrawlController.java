package com.luoxue.mito.controller;

import com.luoxue.mito.common.ResultBody;
import com.luoxue.mito.crawler.VmGirlsCrawlType;
import com.luoxue.mito.crawler.VmGirlsCrawler;
import com.luoxue.mito.service.IVmGirlsCrawService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/vmGirlsCrawl")
public class VmGirlsCrawlController {
    @Resource
    private IVmGirlsCrawService vmGirlsCrawService;

    @GetMapping
    public ResponseEntity<?> start() {
        if (VmGirlsCrawler.getVmGirlsCrawlType() == VmGirlsCrawlType.start) {
            VmGirlsCrawler.setVmGirlsCrawlType(VmGirlsCrawlType.running);
            vmGirlsCrawService.vmGirlsCrawlRun();
        }
        return ResponseEntity.ok(VmGirlsCrawler.getVmGirlsCrawlType());
    }

    @GetMapping("/restart")
    public ResponseEntity<?> restart() {
        VmGirlsCrawler.setVmGirlsCrawlType(VmGirlsCrawlType.start);
        return start();
    }
}
