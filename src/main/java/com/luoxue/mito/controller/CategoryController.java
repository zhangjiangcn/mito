package com.luoxue.mito.controller;

import com.luoxue.mito.dao.CategoryRepository;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;

@RepositoryRestController
public class CategoryController {
    private static final String baseUrl = "/categories";

    @Resource
    private CategoryRepository repository;

    @RequestMapping(value = baseUrl + "/getAllCategories", method = RequestMethod.GET)
    public ResponseEntity<?> getAllCategories() {
        return ResponseEntity.ok(repository.getAllCategory());
    }

    @RequestMapping(value = {baseUrl + "/getTags"}, method = RequestMethod.GET)
    public ResponseEntity<?> getTags(@RequestParam(required = false) String category) {
        return ResponseEntity.ok(repository.getTags(category));
    }

    @RequestMapping(value = {baseUrl + "/list"}, method = RequestMethod.GET)
    public ResponseEntity<?> getList() {
        return ResponseEntity.ok(repository.list());
    }
}
