package com.luoxue.mito.controller;

import com.luoxue.mito.dao.UserRepository;
import com.luoxue.mito.entity.User;
import com.luoxue.mito.util.MitoUtil;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;

@RepositoryRestController
public class UserController {
    @Resource
    private UserRepository repository;

    @RequestMapping(value = "/users/list", method = RequestMethod.GET)
    public ResponseEntity<?> list(User user) {
        ExampleMatcher exampleMatcher = ExampleMatcher.matching()
                .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.contains())
                .withMatcher("age", ExampleMatcher.GenericPropertyMatchers.exact());
        Page<User> page = MitoUtil.findAll(repository, user, exampleMatcher);
        return ResponseEntity.ok(page);
    }
}
