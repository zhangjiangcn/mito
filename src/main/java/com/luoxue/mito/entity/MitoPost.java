package com.luoxue.mito.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.luoxue.mito.util.GsonUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class MitoPost extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String originUrl;
    private String title;
    private String category;

    private String summary;
    private String pic;

    private String serverPic;
    @Column(columnDefinition = "text")
    private String serverImages;

    @Column(columnDefinition = "text")
    private String images;
    @Column(columnDefinition = "text")
    private String tags;

    private Integer pageViews;
    private Integer dataFlag;

//    @Transient
//    private List<String> imageList;
//    @Transient
//    private List<String> tagList;

    public MitoPost(String originUrl, String title, String category, String summary, String pic, Integer pageViews, Integer dataFlag, List<String> imageList, List<String> tagList) {
        this.originUrl = originUrl;
        this.title = title;
        this.category = category;
        this.summary = summary;
        this.pic = pic;
        this.pageViews = pageViews;
        this.dataFlag = dataFlag;
//        this.imageList = imageList;
//        this.tagList = tagList;
        this.images = GsonUtil.GsonString(imageList);
        this.tags = GsonUtil.GsonString(tagList);
    }
}
