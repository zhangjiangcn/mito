package com.luoxue.mito.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.io.Serializable;

@Data
public class BaseEntity implements Serializable {
    @JsonIgnore
    private Integer pageNum;
    @JsonIgnore
    private Integer pageSize;
    @JsonIgnore
    private String sortField;
}
