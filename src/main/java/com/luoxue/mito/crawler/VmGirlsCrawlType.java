package com.luoxue.mito.crawler;

import lombok.Getter;

@Getter
public enum VmGirlsCrawlType {
    start,
    running,
    done;
}
