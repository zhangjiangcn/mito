package com.luoxue.mito.config;

import lombok.Data;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.net.InetAddress;
import java.net.UnknownHostException;


@ConfigurationProperties(prefix = "mito")
@Data
public class MitoConfig {
    @Value("/temps/vmgirls")
    private String filePath;
    @Value("/file")
    private String filePathPattern;
    private String fileBaseUrl;
    @Value("true")
    private Boolean enableLocalUpload;

    @Value("${server.port}")
    private int serverPort;

    public String getFileBaseUrl() {
        if(fileBaseUrl == null){
            this.fileBaseUrl = getUrl();
        }
        return fileBaseUrl+ filePathPattern;
    }

    public String getUrl() {
        try {
            InetAddress address = InetAddress.getLocalHost();
            return "http://" + address.getHostAddress() + ":" + this.serverPort;
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return "";
    }
}
