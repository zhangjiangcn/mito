package com.luoxue.mito.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;
import java.io.File;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Resource
    private MitoConfig mitoConfig;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(mitoConfig.getFilePathPattern() + "/**").addResourceLocations("file:" + mitoConfig.getFilePath() + File.separator);
    }
}
