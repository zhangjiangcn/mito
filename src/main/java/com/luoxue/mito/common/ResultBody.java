package com.luoxue.mito.common;

import lombok.Data;

@Data
public class ResultBody {
    private Integer code;
    private String msg;
    private Object data;

    public static Integer SUCCESS_CODE = 200;
    public static Integer ERROR_CODE = 301;
    public static Integer ERROR2_CODE = 302;

    public ResultBody() {
    }

    public ResultBody(Integer code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static ResultBody ok(Object data) {
        return ResultBody.ok(null, data);
    }

    public static ResultBody ok(String msg, Object data) {
        return ResultBody.ok(SUCCESS_CODE, msg, data);
    }

    public static ResultBody ok(Integer code, String msg, Object data) {
        return new ResultBody(code, msg, data);
    }

    public static ResultBody error(String msg) {
        return ResultBody.error(ERROR_CODE, msg, null);
    }

    public static ResultBody error(Object data) {
        return ResultBody.error(ERROR2_CODE, null, data);
    }

    public static ResultBody error(Integer code, String msg, Object data) {
        return new ResultBody(code, msg, data);
    }

}
