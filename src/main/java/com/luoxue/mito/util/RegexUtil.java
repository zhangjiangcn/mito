package com.luoxue.mito.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtil {
    public static String matcherText(String regex, String page) {
        Matcher matcher = Pattern.compile(regex).matcher(page);
        if (matcher.find() && matcher.groupCount() > 0) {
            return matcher.group(1);
        }
        return null;
    }

    public static Integer matcherInt(String regex, String page) {
        return Integer.valueOf(matcherText(regex, page));
    }
}
