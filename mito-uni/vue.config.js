const webpack = require('webpack')

const domain = "'http://192.168.0.105:7001'"
// const domain = "'http://192.168.0.104:7001'"
// const domain = "'http://192.168.0.146:7001'"

module.exports = {
	chainWebpack: config => {
		config
			.plugin('define')
			.tap(args => {
				args[0]['process.env'].VUE_APP_DOMAIN = domain
				return args
			})
	}
}
