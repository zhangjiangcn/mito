export function list(params) {
	return uni.$u.http.get('mitoPosts/list', {
		params
	})
}

export function get(id) {
	return uni.$u.http.get('mitoPosts/' + id)
}
