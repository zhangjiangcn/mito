import { isDev } from "../utils"

const baseURL = `${process.env.VUE_APP_DOMAIN}/api`
export const imageBaseURL = `${process.env.VUE_APP_DOMAIN}/vmgirls`

export default {

	// #ifdef H5
	baseURL: '/api',
	// #endif

	// #ifndef H5
	baseURL,
	// #endif
}
