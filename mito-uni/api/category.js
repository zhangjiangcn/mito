export function list(params) {
	return uni.$u.http.get('categories/list', {
		params
	})
}