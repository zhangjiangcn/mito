import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user.js'
import search from './modules/search'
import createPersistedState from "vuex-persistedstate";
import storage from "../utils/storage/index.js";
import {
	isDev
} from "../utils";


Vue.use(Vuex);
const store = new Vuex.Store({
	modules: {
		user,
		search
	},
	strict: isDev(),
	plugins: [
		createPersistedState({
			key: "mito-vuex",
			storage,
			paths: ["user", "search"]
		})
	]
})

export default store
