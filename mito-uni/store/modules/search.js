// initial state
// shape: [{ id, quantity }]
const state = () => ({
	params: {
		title: "",
		category: "",
		tags: "",
		sortField: ""
	}
})

// getters
const getters = {

}

// mutations
const mutations = {
	setParams(state, params) {
		state.params = params
	}
}
// actions
const actions = {
	// setParams({
	// 	commit
	// }, params) {
	// 	commit('setParams', params)
	// }
}



export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
}
