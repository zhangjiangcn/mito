// initial state
// shape: [{ id, quantity }]
const state = () => ({
	token: "token***"
})

// getters
const getters = {

}

// mutations
const mutations = {
	setToken(state, token) {
		state.token = token
	}
}
// actions
const actions = {
	setToken({commit}, token) {
		commit('setToken', token)
	}
}



export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
}
