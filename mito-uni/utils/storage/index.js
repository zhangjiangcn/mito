let storage = {
	setItem(key, value) {
		return uni.setStorageSync(key, value)
	},
	getItem(key) {
		return uni.getStorageSync(key)
	},
	removeItem(key) {
		return uni.removeStorageSync(key)
	},
	info() {
		return uni.getStorageInfoSync()
	},
	clear() {
		uni.clearStorageSync()
	}
}
export default storage;
