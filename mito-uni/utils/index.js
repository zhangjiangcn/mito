export function isDev() {
	return process.env.NODE_ENV !== 'production'
}
export function isProd() {
	return !isDev()
}

const arr = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"]


import randomColor from "randomcolor"

export function getRandomColor(options = {}) {
	return randomColor({
		luminosity: "bright",
		...options
	});
}
