import Vue from 'vue'

import store from './store'
import storage from 'utils/storage/index.js';
import uView from 'uni_modules/uview-ui'

// 引入全局uView
Vue.use(uView)

Vue.prototype.$store = store
Vue.prototype.$storage = storage
